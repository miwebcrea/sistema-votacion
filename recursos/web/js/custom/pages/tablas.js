/**
 * Clase que contiene los metodos que dan funcionalidad a la interfaz
 *
 * @package miwebcrea
 * @author Michael Quiroz
 * @copyright MiWebCrea
 * @link https://www.miwebcrea.cl
 * @since 29/12/2023
 * @version 1.0
 * @filesource pages/tablas.js
*/
$(document).ready(function() {
    /**
     * Metodo que obtiene los datos de la tabla de candidatos
     *
     * @author Michael Quiroz
     * @since 03-11-2021
     * @version 1.0
     *
     * @return
    */
    function cargarDatosTablaCandidatos()
    {
        var data = {
            funcion: 'obtenerCandidatosTodos'
        };
        $.ajax({
            type: "GET",
            url: "./Controlador/Candidatos.php",
            dataType: "json",
            data : data,
            cache: false,
            success:
            function(data)
            {
                $('#tbodyCandidatos').empty();
                var t = "";
                var numerador = 0;
                for (var i = 0; i < data.array.length; i++){
                    numerador = numerador+1;
                    var tr = "";
                    tr += "<tr>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['_id']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_region']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_comuna']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['nombre']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['apellido_paterno']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['apellido_materno']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['rut']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fecha_nacimiento']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['titulo_profesional']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['ocupacion']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['direccion_particular']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['correo_electronico']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['telefono']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['avatar']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['estado']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['creado_en']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['modificado_el']+"</span></td>";
                    tr += "</>";
                    t += tr;
                }

                $('#tableCandidatos').find('tbody').append(t);
            }
        });
    }
    cargarDatosTablaCandidatos();

    /**
     * Metodo que obtiene los datos de la tabla de participantes
     *
     * @author Michael Quiroz
     * @since 03-11-2021
     * @version 1.0
     *
     * @return
    */
    function cargarDatosTablaParticipantes()
    {
        var data = {
            funcion: 'obtenerparticipantesTodos'
        };
        $.ajax({
            type: "GET",
            url: "./Controlador/Participantes.php",
            dataType: "json",
            data : data,
            cache: false,
            success:
            function(data)
            {
                $('#tbodyParticipantes').empty();
                var t = "";
                var numerador = 0;
                for (var i = 0; i < data.array.length; i++){
                    numerador = numerador+1;
                    var tr = "";
                    tr += "<tr>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['_id']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_region']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_comuna']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['nombre']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['apellido_paterno']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['apellido_materno']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['alias']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['rut']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['correo_electronico']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['creado_en']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['modificado_el']+"</span></td>";
                    tr += "</>";
                    t += tr;
                }

                $('#tableParticipantes').find('tbody').append(t);
            }
        });
    }
    cargarDatosTablaParticipantes();

    /**
     * Metodo que obtiene los datos de la tabla de las votaciones
     *
     * @author Michael Quiroz
     * @since 03-11-2021
     * @version 1.0
     *
     * @return
    */
    function cargarDatosTablaVoltaciones()
    {
        var data = {
            funcion: 'obtenerVotacionesTodas'
        };
        $.ajax({
            type: "GET",
            url: "./Controlador/Votacion.php",
            dataType: "json",
            data : data,
            cache: false,
            success:
            function(data)
            {
                $('#tbodyVotacion').empty();
                var t = "";
                var numerador = 0;
                for (var i = 0; i < data.array.length; i++){
                    numerador = numerador+1;
                    var tr = "";
                    tr += "<tr>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['_id']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_participante']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_candidato']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['como_se_entero']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['creado_en']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['modificado_el']+"</span></td>";
                    tr += "</>";
                    t += tr;
                }

                $('#tableVotacion').find('tbody').append(t);
            }
        });
    }
    cargarDatosTablaVoltaciones();

    /**
     * Metodo que obtiene los datos de la tabla de regiones
     *
     * @author Michael Quiroz
     * @since 03-11-2021
     * @version 1.0
     *
     * @return
    */
    function cargarDatosTablaRegiones()
    {
        var data = {
            funcion: 'obtenerRegionesTodas'
        };
        $.ajax({
            type: "GET",
            url: "./Controlador/Ubicacion.php",
            dataType: "json",
            data : data,
            cache: false,
            success:
            function(data)
            {
                $('#tbodyRegiones').empty();
                var t = "";
                var numerador = 0;
                for (var i = 0; i < data.array.length; i++){
                    numerador = numerador+1;
                    var tr = "";
                    tr += "<tr>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['_id']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['cod_region']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['nombre_corto']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['nombre_largo']+"</span></td>";
                    tr += "</>";
                    t += tr;
                }

                $('#tableRegiones').find('tbody').append(t);
            }
        });
    }
    cargarDatosTablaRegiones();

    /**
     * Metodo que obtiene los datos de la tabla de regiones
     *
     * @author Michael Quiroz
     * @since 03-11-2021
     * @version 1.0
     *
     * @return
    */
    function cargarDatosTablaComunas()
    {
        var data = {
            funcion: 'obtenerComunasTodas'
        };
        $.ajax({
            type: "GET",
            url: "./Controlador/Ubicacion.php",
            dataType: "json",
            data : data,
            cache: false,
            success:
            function(data)
            {
                $('#tbodyComunas').empty();
                var t = "";
                var numerador = 0;
                for (var i = 0; i < data.array.length; i++){
                    numerador = numerador+1;
                    var tr = "";
                    tr += "<tr>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['_id']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['fk_region']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['cod_comuna']+"</span></td>";
                        tr += "<td><span class='text-gray-900 fw-bold d-block fs-6'>"+data.array[i]['nombre']+"</span></td>";
                    tr += "</>";
                    t += tr;
                }

                $('#tableComunas').find('tbody').append(t);
            }
        });
    }
    cargarDatosTablaComunas();
});