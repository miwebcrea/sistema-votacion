/**
 * Clase que contiene los metodos que dan funcionalidad a la interfaz
 *
 * @package miwebcrea
 * @author Michael Quiroz
 * @copyright MiWebCrea
 * @link https://www.miwebcrea.cl
 * @since 28-12-2023
 * @version 1.0
 * @filesource pages/votacion.js
*/
$(document).ready(function() {

    /**
     * Metodo que carga las regiones del select selectRegion
     *
     * @author Michael Quiroz
     * @since 28-12-2023
     * @version 1.0
     *
     * @return
    */
    function cargarRegiones() {
        var data = {
            funcion: 'obtenerRegiones'
        };

        $.ajax({
            url : "./Controlador/Ubicacion.php",
            type : "GET",
            dataType:"json",
            data : data,
            cache: false,
            success : function(data) {
                if(data.estado_funcion == 'ok'){
                    var regiones = data.array;
                    for (let index = 0; index < regiones.length; index++) {
                        var cod = regiones[index]['cod_region'];
                        $('#selectRegion').append($('<option>', {
                            value: regiones[index]['_id'],
                            text : regiones[index]['nombre_largo']
                        }).attr('data-kt-select2-region', cod));
                    }
                    $('#selectRegion').select2();
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.hideLoading();
                Swal.fire("!Opps ", "Algo ha salido mal, vuelve a intentarlo en otra ocasión", "error");
            }
        });
    }

    /**
     * Metodo que carga las comunas por la region seleccionada
     *
     * @author Michael Quiroz
     * @since 28-12-2023
     * @version 1.0
     *
     * @return
    */
    function cargarComunas() {
        var data = {
            funcion: 'obtenerComunasPorRegion',
            region: $('#selectRegion').find(':selected').attr("data-kt-select2-region")
        };

        $.ajax({
            url : "./Controlador/Ubicacion.php",
            type : "GET",
            dataType:"json",
            data : data,
            cache: false,
            success : function(data) {
                if(data.estado_funcion == 'ok'){
                    $('#selectComuna').empty().trigger("change");
                    var comunas = data.array;
                    for (let index = 0; index < comunas.length; index++) {
                        $('#selectComuna').append($('<option>', {
                            value: comunas[index]['_id'],
                            text : comunas[index]['nombre']
                        }));
                    }
                    $('#selectComuna').select2();
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.hideLoading();
                Swal.fire("!Opps ", "Algo ha salido mal, vuelve a intentarlo en otra ocasión", "error");
            }
        });
    }

    /**
     * Metodo que carga a los candidatos habilitados para su seleccion
     *
     * @author Michael Quiroz
     * @since 29-12-2023
     * @version 1.0
     *
     * @return
    */
    function cargarCandidatos() {
        var data = {
            funcion: 'obtenerCandidatosSelect'
        };

        $.ajax({
            url : "./Controlador/Candidatos.php",
            type : "GET",
            dataType:"json",
            data : data,
            cache: false,
            success : function(data) {
                if(data.estado_funcion == 'ok'){
                    var candidatos = data.array;
                    for (let index = 0; index < candidatos.length; index++) {
                        var avatar = candidatos[index]['avatar'];
                        $('#selectCandidato').append($('<option>', {
                            value: candidatos[index]['_id'],
                            text : candidatos[index]['candidato']
                        }).attr('data-kt-select2-avatar', avatar));
                    }
                    const format = (item) => {
                        if (!item.id) {
                            return item.text;
                        }
                        var url = 'recursos/web/media/avatars/' + item.element.getAttribute('data-kt-select2-avatar');
                        var img = $("<img>", {
                            class: "rounded-circle me-2",
                            width: 26,
                            src: url
                        });
                        var span = $("<span>", {
                            text: " " + item.text
                        });
                        span.prepend(img);
                        return span;
                    }
                    $('#selectCandidato').select2({
                        sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
                        templateResult: function (item) {
                            return format(item);
                        }
                    });
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.hideLoading();
                Swal.fire("!Opps ", "Algo ha salido mal, vuelve a intentarlo en otra ocasión", "error");
            }
        });
    }

    /* Metodos que son utilizados al cargar la interface */
    cargarRegiones();
    cargarCandidatos();

    /* Metodos que son utilizados al ser gatillados por un evento */
    $('#selectRegion').on('change', function(){
        cargarComunas();
    });

    /* Variables y inicializacion de estados del formulario */
    Inputmask({
		greedy: false,
		casing: 'lower',
		cardinality: 1
	}).mask("#correo");

	Inputmask({
		mask: "9{1,2}.9{3}.9{3}-(9|k|K)",
		greedy: false,
		casing: 'upper',
		clearIncomplete: true,
		numericInput: true,
		positionCaretOnClick: 'none'
	}).mask("#rut");

});