/**
 * Clase que contiene los metodos que dan funcionalidad a la interfaz
 *
 * @package miwebcrea
 * @author Michael Quiroz
 * @copyright MiWebCrea
 * @link https://www.miwebcrea.cl
 * @since 28-12-2023
 * @version 1.0
 * @filesource votacion-form.js
*/

$(document).ready(function() {
    /**
     * Definimos las validaciones para el fomulario
     *
     * @author Michael Quiroz
     * @since 28-12-2023
     * @version 1.0
     *
     * @return
    */
    const form = document.getElementById('votacion_form');

    // Revisamos que el RUT no este ingresado en el sistema
	const revisarRut = function() {
		var data = {
			funcion: 'verificarExistenciaRut',
			rut: $("#rut").val()
		};
		$.ajax({
			type : "GET",
			url : "./Controlador/Participantes.php",
			dataType: "json",
			data : data,
			cache: false,
			success : function(data) {
				if(data.estado == 'existe'){
					// Cambiamos el valor del input oculta para gatillar la alerta
					$('#validRut').val(1);
				} else {
					$('#validRut').val(0);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				Swal.hideLoading();
				Swal.fire("!Opps ", "Algo ha salido mal, vuelve a intentarlo en otra ocasión", "error");
			}
		});
		return {
			validate: function(input) {
				const validRut = $('#validRut').val();
				if (validRut == 1) {
					return {
						valid: false
					};
				} else {
					return {
						valid: true
					};
				}
			},
		};
	};

	// Revisamos que el correo no este ingresado en el sistema
	const revisarCorreo = function() {
		var data = {
			funcion: 'verificarExistenciaCorreo',
			correo: $("#correo").val()
		};
		$.ajax({
			type : "GET",
			url : "./Controlador/Participantes.php",
			dataType: "json",
			data : data,
			cache: false,
			success : function(data) {
				if(data.estado == 'existe'){
					// Cambiamos el valor del input oculta para gatillar la alerta
					$('#validCorreo').val(1);
				} else {
					$('#validCorreo').val(0);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				Swal.hideLoading();
				Swal.fire("!Opps ", "Algo ha salido mal, vuelve a intentarlo en otra ocasión", "error");
			}
		});
		return {
			validate: function(input) {
				const validCorreo = $('#validCorreo').val();
				if (validCorreo == 1) {
					return {
						valid: false
					};
				} else {
					return {
						valid: true
					};
				}
			},
		};
	};

    // Revisamos que el checkbox de como 'como se entero' tenga al menos 2 opciones marcadas
    const revisarCheckbox = function() {
        var checkboxes = $('input[name=checkbox_como_llego]');
        var array_checkbox = Array.from(checkboxes).filter(
            checkbox => checkbox.checked,
        );
        var checkboxs_marcados = array_checkbox.length;
        return {
			validate: function(input) {
				if (checkboxs_marcados <= 1) {
					return {
						valid: false
					};
				} else {
					return {
						valid: true
					};
				}
			},
		};
    };

    // Revisamos que el alias tenga al menos una letra y numero
    const revisarAlias = function() {
        var alias = $('#alias').val();
        var contiene_numero = 0;
        var contiene_letra = 0;
        // Analizamos si el string de alias contiene número
        if(alias.search(/\d/) === -1) {
            contiene_numero = 0;
        } else {
            contiene_numero = 1;
        }
        if (alias.match(/[a-zA-Z]/g)) {
            contiene_letra = 1;
        }

        return {
			validate: function(input) {
				if (contiene_letra == 1 && contiene_numero == 1) {
					return {
						valid: true
					};
				} else {
					return {
						valid: false
					};
				}
			},
		};
    }

    // Revisamos que el alias NO tenga caracteres especiales
    const revisarCaracteresEspeciales = function() {
        const caracteres_especiales = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
        return {
			validate: function(input) {
				if (caracteres_especiales.test(input.value)) {
					return {
						valid: false
					};
				} else {
					return {
						valid: true
					};
				}
			},
		};
    }

    // Insertamos la validacion al form
	FormValidation.validators.revisarRut = revisarRut;
	FormValidation.validators.revisarCorreo = revisarCorreo;
    FormValidation.validators.revisarCheckbox = revisarCheckbox;
    FormValidation.validators.revisarAlias = revisarAlias;
    FormValidation.validators.revisarCaracteresEspeciales = revisarCaracteresEspeciales;

    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                'nombre': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar tu nombre'
                        },
                        stringLength: {
                            max: 50,
                            message: 'No puedes ingresar más de 50 caracteres',
                        },
                        revisarCaracteresEspeciales: {
                            message: 'No debes ingresar caracteres especiales'
                        }
                    }
                },
                'apellido_paterno': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar tu apellido paterno'
                        },
                        stringLength: {
                            max: 100,
                            message: 'No puedes ingresar más de 100 caracteres',
                        },
                        revisarCaracteresEspeciales: {
                            message: 'No debes ingresar caracteres especiales'
                        }
                    }
                },
                'apellido_materno': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar tu apellido materno'
                        },
                        stringLength: {
                            max: 100,
                            message: 'No puedes ingresar más de 100 caracteres',
                        },
                        revisarCaracteresEspeciales: {
                            message: 'No debes ingresar caracteres especiales'
                        }
                    }
                },
                'alias': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar tu alias'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Debes ingresar al menos 5 caracteres',
                        },
                        revisarAlias: {
                            message: 'Debes ingresar al menos una letra y número'
                        },
                        revisarCaracteresEspeciales: {
                            message: 'No debes ingresar caracteres especiales'
                        }
                    }
                },
                'rut': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar tu RUT'
                        },
                        revisarRut: {
                            message: 'El RUT ingresado, ya existe en nuestro sistema.'
                        }
                    }
                },
                'correo': {
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar tu correo electrónico personal'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[com|co|cl|in|org|net|edu|es|info|gov|gob|pe|ve]+$',
                            message: 'Debes ingresar un correo electrónico válido'
                        },
                        stringLength: {
                            max: 60,
                            message: 'No puedes ingresar más de 60 caracteres',
                        },
                        revisarCorreo: {
                            message: 'El correo electrónico ingresado, ya existe en nuestro sistema.'
                        }
                    }
                },
                'selectRegion': {
                    validators: {
                        notEmpty: {
                            message: 'Debes seleccionar la región a la cual perteneces'
                        }
                    }
                },
                'selectComuna': {
                    validators: {
                        notEmpty: {
                            message: 'Debes seleccionar la comuna a la cual perteneces'
                        }
                    }
                },
                'selectCandidato': {
                    validators: {
                        notEmpty: {
                            message: 'Debes seleccionar un candidato'
                        }
                    }
                },
                'checkbox_como_llego': {
                    validators: {
                        notEmpty: {
                            message: 'Debes indicarnos como nos conociste'
                        },
                        revisarCheckbox: {
                            message: 'Debes marcar al menos dos opciones'
                        }
                    }
                }
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger({
                    event: {
                        nombre: 'blur',
                        apellido_paterno: 'blur',
                        apellido_materno: 'blur',
                        alias: 'blur change',
                        rut: 'blur',
                        correo: 'blur',
                        selectRegion: 'blur',
                        selectComuna: 'blur',
                        selectCandidato: 'blur',
                        checkbox_como_llego: 'blur change'
                    }
                }),
                bootstrap: new FormValidation.plugins.Bootstrap5({
                    rowSelector: '.fv-row',
                    eleInvalidClass: '',
                    eleValidClass: ''
                })
            }
        }
    );

    // Evento del boton submit
    const submitButton = document.getElementById('votacion_submit');
    submitButton.addEventListener('click', function (e) {
        e.preventDefault();

        // Validamos el formulario antes del submit
        if (validator) {
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    submitButton.setAttribute('data-kt-indicator', 'on');
                    submitButton.disabled = true;
                    // Ejecutamos el AJAX para modificar la BD
                    var form = $("#votacion_form").serializeArray();
                    var formData = {
                        funcion: 'crearVotacion',
                        datos: form
                    };

                    $.ajax({
                        type: "POST",
                        url: "./Controlador/Votacion.php",
                        data: formData,
                        dataType: "json",
                        cache: false,
                        success:
                        function(data)
                        {
                            if(data.estado_funcion == 'ok') {
                                Swal.fire({
                                    text: data.mensaje,
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: 'Entiendo',
                                    timer: 5000,
                                    timerProgressBar: true,
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(() => {
                                    window.location.href = data.url;
                                });
                            }
                            else if(data.estado_funcion == 'error'){
                                Swal.fire({
                                    text: data.mensaje,
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: 'Entiendo',
                                    timer: 5000,
                                    timerProgressBar: true,
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(() => {
                                    location.reload();
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            Swal.hideLoading();
                            Swal.fire("!Opps ", "Algo ha salido mal, vuelve a intentarlo en otra ocasión", "error");
                        },
                        complete: function() {
                            submitButton.removeAttribute('data-kt-indicator');
                            submitButton.disabled = false;
                        }
                    });
                } else {
                    Swal.fire({
                        text: "Ops, no puedes continuar hasta que ingreses correctamente los datos solicitados, intenta nuevamente.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, continuemos!",
                        customClass: {
                            confirmButton: "btn btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
        }
    });

});