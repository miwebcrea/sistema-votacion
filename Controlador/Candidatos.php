<?php
    // SECTION - Requeridos
    require_once "Validaciones.php";
    require_once "../Modelo/Votacion_model.php";
    require_once "../Modelo/Participante_model.php";
    require_once "../Modelo/Candidato_model.php";
    require_once "Parametros_globales.php";
    mb_internal_encoding('UTF-8');
    # Establecemos la hora local
    date_default_timezone_set('America/Santiago');
    // !SECTION
    if(!empty($_GET)){
        // SECTION - Metodos GET
        $metodoGET = $_GET['funcion'];
        switch ($metodoGET) {
            // SECTION - obtenerCandidatosSelect
            case 'obtenerCandidatosSelect':
                // REVIEW - Consultamos a la base de datos para obtener un arreglo con todos los candidatos que estan habilitados para la votacion
                // NOTE - Instancia para inicializar los modelos a usar
                $candidato_model = new Candidato_model;
                $validaciones = new Validaciones;
                $arrayCandidatos = $candidato_model->getCandidatosTodos();
                $arrayRespuesta = [];
                // NOTE - Recorremos los registros de los candidatops y los formateamos para la salida a la interface
                foreach ($arrayCandidatos as $index => $candidato) {
                    // NOTE - Mostramos solo los candidatos habilitados
                    if($candidato['estado'] == 1){
                        // NOTE - Formateamos la información para mostrarla en la interface
                        $arrayRespuesta[$index]['candidato'] = $arrayCandidatos[$index]['nombre'].' '.$arrayCandidatos[$index]['apellido_paterno'].' '.$arrayCandidatos[$index]['apellido_materno']. ' | '.$arrayCandidatos[$index]['titulo_profesional'];
                        $arrayRespuesta[$index]['_id'] = $validaciones->encriptarID($arrayCandidatos[$index]['_id'], 'getDatosCandidatoKey');
                        $arrayRespuesta[$index]['avatar'] = $arrayCandidatos[$index]['avatar'];
                    }
                }
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayRespuesta
                );
                echo json_encode($respuesta);
                return true;

            break;
            // !SECTION

            // SECTION - obtenerCandidatosTodos
            case 'obtenerCandidatosTodos':
                // REVIEW - Consultamos a la base de datos para obtener un arreglo con todos los candidatos sin filtros
                // NOTE - Instancia para inicializar los modelos a usar
                $candidato_model = new Candidato_model;
                $validaciones = new Validaciones;
                $arrayCandidatos = $candidato_model->getCandidatosTodos();
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayCandidatos
                );
                echo json_encode($respuesta);
                return true;

            break;
            // !SECTION
        }
        // !SECTION
    }else{
        // SECTION - Metodos POST
        $metodoPOST = $_POST['funcion'];
        switch ($metodoPOST) {
        }
        // !SECTION
    }
?>