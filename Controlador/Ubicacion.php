<?php
    // SECTION - Requeridos
    require_once "Validaciones.php";
    require_once "../Modelo/Ubicacion_model.php";
    require_once "Parametros_globales.php";
    mb_internal_encoding('UTF-8');
    // !SECTION
    if(!empty($_GET)){
        // SECTION - Metodos GET
        $metodoGET = $_GET['funcion'];
        switch ($metodoGET) {
            // SECTION - obtenerRegiones
            case 'obtenerRegiones':
                // REVIEW - Obtenemos el array de todas las regiones de la base de datos para ser mostradas en la iterface de usuario
                // NOTE - Instanciamos el modelo de paises
                $ubicacion_model = new Ubicacion_model;
                $validaciones = new Validaciones;
                $arrayRegiones = $ubicacion_model->getRegionesTodas();
                // NOTE - Encriptamos el ID
                foreach ($arrayRegiones as $index => $regiones) {
                    $arrayRegiones[$index]['_id'] = $validaciones->encriptarID($arrayRegiones[$index]['_id'], 'getDatosRegionesKey');
                }
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayRegiones
                );
                echo json_encode($respuesta);
                return true;
            break;
            // !SECTION
            // SECTION - obtenerComunasPorRegion
            case 'obtenerComunasPorRegion':
                // REVIEW - Obtenemos un array de las comunas, filtradas por una region en particular.
                // NOTE - Instanciamos el modelo de paises
                $ubicacion_model = new Ubicacion_model;
                $validaciones = new Validaciones;
                $region = $ubicacion_model->getRegionPorCod($_GET['region']);
                $id_region = intval($region[0]['_id']);
                $arrayComunas = $ubicacion_model->getComunaPorRegion($id_region);
                // NOTE - Encriptamos los IDs
                foreach ($arrayComunas as $index => $regiones) {
                    $arrayComunas[$index]['_id'] = $validaciones->encriptarID($arrayComunas[$index]['_id'], 'getDatosComunasKey');
                    $arrayComunas[$index]['fk_region'] = $validaciones->encriptarID($arrayComunas[$index]['fk_region'], 'getDatosComunasKey');
                }
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayComunas
                );
                echo json_encode($respuesta);
                return true;
            break;
            // !SECTION
            // SECTION - obtenerRegionesTodas
            case 'obtenerRegionesTodas':
                // REVIEW - Consultamos a la base de datos para obtener un arreglo con todas las regiones sin filtro y encriptaciones
                // NOTE - Instancia para inicializar los modelos a usar
                $ubicacion_model = new Ubicacion_model;
                $arrayRegiones = $ubicacion_model->getRegionesTodas();
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayRegiones
                );
                echo json_encode($respuesta);
                return true;

            break;
            // !SECTION
            // SECTION - obtenerComunasTodas
            case 'obtenerComunasTodas':
                // REVIEW - Consultamos a la base de datos para obtener un arreglo con todas las comunas sin filtro y encriptaciones
                // NOTE - Instancia para inicializar los modelos a usar
                $ubicacion_model = new Ubicacion_model;
                $arrayComunas = $ubicacion_model->getComunasTodas();
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayComunas
                );
                echo json_encode($respuesta);
                return true;

            break;
            // !SECTION
        }
        // !SECTION
    }else{
        // SECTION - Metodos POST
        $metodoPOST = $_POST['funcion'];
        switch ($metodoPOST) {
        }
        // !SECTION
    }
?>