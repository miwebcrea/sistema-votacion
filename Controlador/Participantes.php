<?php
    require_once "Validaciones.php";
    require_once "../Modelo/Participante_model.php";
    require_once "Parametros_globales.php";
    # Establecemos la hora local
    date_default_timezone_set('America/Santiago');
    mb_internal_encoding('UTF-8');
    session_start();

    if(!empty($_GET)){
        # Metodos GET
        $metodoGET = $_GET['funcion'];
        switch ($metodoGET) {
            // SECTION - verificarExistenciaRut
            case 'verificarExistenciaRut':
                # Instancia para inicializar el modelo del participante
                $participante_model = new Participante_model;
                $arrayParticipante = $participante_model->getParticipantePorRut($_GET['rut']);

                if(count($arrayParticipante) > 0) {
                    $respuesta = array(
                        'estado'        => 'existe'
                    );

                    echo json_encode($respuesta);
                    return true;

                } else {
                    $respuesta = array(
                        'estado'        => 'noexiste'
                    );

                    echo json_encode($respuesta);
                    return true;
                }
            break;
            // !SECTION
             // SECTION - verificarExistenciaCorreo
            case 'verificarExistenciaCorreo':
                # Instancia para inicializar el modelo del participante
                $participante_model = new Participante_model;
                $arrayParticipante = $participante_model->getUsuarioPorCorreo($_GET['correo']);

                if(count($arrayParticipante) > 0) {
                    $respuesta = array(
                        'estado'        => 'existe'
                    );

                    echo json_encode($respuesta);
                    return true;

                } else {
                    $respuesta = array(
                        'estado'        => 'noexiste'
                    );

                    echo json_encode($respuesta);
                    return true;
                }
            break;
            // !SECTION
            // SECTION - obtenerparticipantesTodos
            case 'obtenerparticipantesTodos':
                // REVIEW - Consultamos a la base de datos para obtener un arreglo con todos los candidatos sin filtros
                // NOTE - Instancia para inicializar los modelos a usar
                $participantes_model = new Participante_model;
                $validaciones = new Validaciones;
                $arrayParticipantes = $participantes_model->getParticipantesTodos();
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayParticipantes
                );
                echo json_encode($respuesta);
                return true;

            break;
            // !SECTION
        }
    } else {
        # Metodos POST
        $metodoPOST = $_POST['funcion'];
        switch ($metodoPOST) {

        }
    }

?>