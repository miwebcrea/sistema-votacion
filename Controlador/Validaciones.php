<?php
class Validaciones
{
    /**
     * Encripta un valor tipo id
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   valor
     * @return  encryption
    */
    public function encriptarID($string, $key)
    {
        $result = '';
        for($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result .= $char;
        }
        return base64_encode($result);
    }

    /**
     * Desencripta un valor tipo id
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   valor
     * @return  decryption
    */
    public function desencriptarID($string, $key)
    {
        $result = '';
        $string = base64_decode($string);
        for($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $result .= $char;
        }
        return $result;
    }

    /**
     * Validamos si el string es un correo valido
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   string
     * @return  boolean
    */
    public function validar_correo($string){
        $string = filter_var($string, FILTER_SANITIZE_EMAIL);
        if(filter_var($string, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Traduccion de los estados de un candidato
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   string
     * @return  string
    */
    public function estado_candidato_texto($estado){
        $estado_texto = "";
        switch (intval($estado)) {
            case 0:
                $estado_texto = "Habilitado";
            break;
            case 1:
                $estado_texto = "Inhabilitado";
                break;
            default:
                $estado_texto = "Inhabilitado";
            break;
        }
        return $estado_texto;
    }

}
?>