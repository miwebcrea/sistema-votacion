<?php
    // SECTION - Requeridos
    require_once "Validaciones.php";
    require_once "../Modelo/Votacion_model.php";
    require_once "../Modelo/Participante_model.php";
    require_once "../Modelo/Candidato_model.php";
    require_once "Parametros_globales.php";
    mb_internal_encoding('UTF-8');
    # Establecemos la hora local
    date_default_timezone_set('America/Santiago');
    // !SECTION
    if(!empty($_GET)){
        // SECTION - Metodos GET
        $metodoGET = $_GET['funcion'];
        switch ($metodoGET) {
            // SECTION - obtenerVotacionesTodas
            case 'obtenerVotacionesTodas':
                // REVIEW - Consultamos a la base de datos para obtener un arreglo con todos los candidatos sin filtros
                // NOTE - Instancia para inicializar los modelos a usar
                $votacion_model = new Votacion_model;
                $arrayVotacion = $votacion_model->getVotacionesTodas();
                // STUB - Array de respuesta
                $respuesta = array(
                    'estado_funcion'    => 'ok',
                    'mensaje'           => 'Elemento obtenido correctamente!',
                    'array'             => $arrayVotacion
                );
                echo json_encode($respuesta);
                return true;

            break;
            // !SECTION
        }
        // !SECTION
    }else{
        // SECTION - Metodos POST
        $metodoPOST = $_POST['funcion'];
        switch ($metodoPOST) {
            // SECTION - crearVotacion
            case 'crearVotacion':
                // REVIEW - Registramos a un nuevo participante de la votación y su voto de manera individual
                // NOTE - Establecemos los datos del formulario de manera estandarizada y formateada para la base de datos
                foreach ($_POST['datos'] as $key) {
                    if($key['name'] == 'nombre') { $nombre_votante = ucfirst(mb_strtolower($key['value'])); }
                    if($key['name'] == 'apellido_paterno') { $apellido_paterno = ucfirst(mb_strtolower($key['value'])); }
                    if($key['name'] == 'apellido_materno') { $apellido_materno = ucfirst(mb_strtolower($key['value'])); }
                    if($key['name'] == 'alias') { $alias = $key['value']; }
                    if($key['name'] == 'rut') { $rut = $key['value']; }
                    if($key['name'] == 'correo') { $correo_electronico = $key['value']; }
                    if($key['name'] == 'fecha_nacimiento') { $fecha_nacimiento = $key['value']; }
                    if($key['name'] == 'selectRegion') { $region = $key['value']; }
                    if($key['name'] == 'selectComuna') { $comuna = $key['value']; }
                    if($key['name'] == 'selectCandidato') { $candidato = $key['value']; }
                }
                // NOTE - Buscamos las opciones maracadas en el checkbox y concatenamos el resultado, ya que el valor contiene al menos 2 resultados
                $buscar = 'checkbox_como_llego';
                $encontrado = array_filter($_POST['datos'],function($v,$k) use ($buscar){
                    return $v['name'] == $buscar;
                },ARRAY_FILTER_USE_BOTH);
                $arreglo_checkbox = array_values($encontrado);
                $valor_checkbox = '';
                foreach ($arreglo_checkbox as $llave) {
                    // NOTE - Concatenamos los valores seleccionados por el usuario votante, para almacenarlos en la base de datos
                    $valor_checkbox .= $llave['value'];
                }
                // NOTE - Desencriptamos los datos sensibles y preparamos el arreglo para la insercion en la base de datos
                $validaciones = new Validaciones;
                $fk_region = $validaciones->desencriptarID($region, 'getDatosRegionesKey');
                $fk_comuna = $validaciones->desencriptarID($comuna, 'getDatosComunasKey');
                $fk_candidato = $validaciones->desencriptarID($candidato, 'getDatosCandidatoKey');
                // NOTE - Primero, creamos el arreglo para crear al participante de la votacion
                $data_participante = array(
                    'fk_region'             => intval($fk_region),
                    'fk_comuna'             => intval($fk_comuna),
                    'nombre'                => $nombre_votante,
                    'apellido_paterno'      => $apellido_paterno,
                    'apellido_materno'      => $apellido_materno,
                    'alias'                 => $alias,
                    'rut'                   => $rut,
                    'correo_electronico'    => $correo_electronico,
                    'creado_en'             => date("d/m/Y H:i"),
                    'modificado_el'         => date("d/m/Y H:i")
                );
                // STUB - Insertamos el nuevo registro de participante en la base de datos
                $participante_model = new Participante_model;
                $resultado_insertParticipante = $participante_model->insertParticipante($data_participante);
                // STUB - Si la insercion resulto correcta, procedemos con la insercion del voto
                if($resultado_insertParticipante == TRUE){
                    // NOTE - Creamos el arreglo para crear el voto de dicho participante
                    $data_votacion = array(
                        'fk_participante'       => intval($resultado_insertParticipante['_id']),
                        'fk_candidato'          => intval($fk_candidato),
                        'como_se_entero'        => $valor_checkbox,
                        'creado_en'             => date("d/m/Y H:i"),
                        'modificado_el'         => date("d/m/Y H:i")
                    );
                    // STUB - Insertamos el nuevo registro de votacion en la base de datos
                    $votacion_model = new Votacion_model;
                    $resultado_insertVotacion = $votacion_model->insertVotacion($data_votacion);
                    $respuesta = array(
                        'estado_funcion'    => 'ok',
                        'mensaje'           => 'Has realizado tu votación de manera correcta, gracias por participar.',
                        'url'               => $url_votacion
                    );
                    echo json_encode($respuesta);
                    return true;
                } else {
                    $respuesta = array(
                        'estado_funcion'    => 'error',
                        'mensaje'           => 'Ha ocurrido un error inesperado, por favor, vuelve a realizar tu votación en otro momento.',
                        'url'               => $url_votacion
                    );
                    echo json_encode($respuesta);
                    return true;
                }
            break;
            // !SECTION
        }
        // !SECTION
    }
?>