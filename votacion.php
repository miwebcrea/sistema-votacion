<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<title>Sistema de votación</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow" />
		<meta property="og:locale" content="es_CL" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Sistema de votación | Miwebcrea" />
		<meta property="og:url" content="https://miwebcrea.cl/integraciones/sistema-votacion/" />
		<meta property="og:site_name" content="Miwebcrea" />
		<link rel="shortcut icon" href="recursos/web/media/logos/favicon.ico" />
		<link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
		<link href="recursos/web/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="recursos/web/css/style.bundle.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="kt_body" class="bg-body">
		<!--begin::Principal-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Votacion-->
			<div class="d-flex flex-column flex-lg-row flex-column-fluid">
				<!--begin::Grid Central-->
				<div class="d-flex flex-column flex-lg-row-fluid py-10">
					<div class="d-flex flex-center flex-column flex-column-fluid">
						<div class="w-lg-700px p-10 p-lg-15 mx-auto">
							<!--begin::Form-->
							<form class="my-auto pb-5" novalidate="novalidate" id="votacion_form">
                                <div class="w-100">
                                    <!--begin::Cabecera-->
                                    <div class="pb-10 pb-lg-15">
                                    </div>
                                    <!--end::Cabecera-->
                                    <!--begin::Campos de datos-->
                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                        <label class="form-label fw-bolder text-dark fs-6 required">Nombre</label>
                                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="nombre" id="nombre" maxlength="100" autocomplete="off">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>

                                    <div class="row fv-row mb-7 fv-plugins-icon-container">
                                        <div class="col-xl-6">
                                            <label class="form-label fw-bolder text-dark fs-6 required">Apellido Paterno</label>
                                            <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="apellido_paterno" id="apellido_paterno" maxlength="100" autocomplete="off">
                                        <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                        <div class="col-xl-6">
                                            <label class="form-label fw-bolder text-dark fs-6 required">Apellido Materno</label>
                                            <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="apellido_materno" id="apellido_materno" maxlength="100" autocomplete="off">
                                        <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                    </div>

                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                        <label class="form-label fw-bolder text-dark fs-6 required">Alias</label>
                                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="alias" id="alias" maxlength="100" autocomplete="off">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>

                                    <div class="fv-row mb-7 fv-plugins-icon-container fv-plugins-bootstrap5-row-invalid">
                                        <label class="form-label fw-bolder text-dark fs-6 required">RUT</label>
                                        <input class="form-control form-control-lg form-control-solid" type="text" name="rut" id="rut" autocomplete="off">
                                        <input id="validRut" class="no-mostrar" type="hidden" value="0">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>

                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                        <label class="form-label fw-bolder text-dark fs-6 required">Correo electrónico</label>
                                        <input class="form-control form-control-lg form-control-solid" type="text" data-inputmask-alias="email" placeholder="" name="correo" id="correo" maxlength="80" autocomplete="off">
                                        <input id="validCorreo" class="no-mostrar" type="hidden" value="0">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>

                                    <div class="row fv-row mb-7 fv-plugins-icon-container">
                                        <div class="col-xl-6">
                                            <label class="form-label fw-bolder text-dark fs-6 required">Región</label>
                                            <div class="input-group input-group-solid">
                                                <div class="flex-grow-1">
                                                    <select class="form-select form-select-solid rounded-start-0 border-start" name="selectRegion" id="selectRegion" data-control="select2" data-placeholder="Selecciona una opción" autocomplete="off">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                        <div class="col-xl-6">
                                            <label class="form-label fw-bolder text-dark fs-6 required">Comuna</label>
                                            <div class="input-group input-group-solid">
                                                <div class="flex-grow-1">
                                                    <select class="form-select form-select-solid rounded-start-0 border-start" name="selectComuna" id="selectComuna" data-control="select2" data-placeholder="Selecciona una opción" autocomplete="off">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="fv-row mb-10 fv-plugins-icon-container">
                                        <div class="col-xl-12">
                                            <label class="form-label fw-bolder text-dark fs-6 required">Candidato</label>
                                            <div class="input-group input-group-solid">
                                                <div class="flex-grow-1">
                                                    <select class="form-select form-select-solid rounded-start-0 border-start" name="selectCandidato" id="selectCandidato" data-control="select2" data-placeholder="Selecciona una opción" autocomplete="off">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="row fv-row mb-7 fv-plugins-icon-container">
                                        <div class="d-flex flex-column fv-row">
                                            <div class="me-5 mb-3">
                                                <label class="form-label fw-bolder text-dark fs-6 required">¿Cómo se entero de nosotros?</label>
                                            </div>
                                            <div class="form-check form-check-custom form-check-solid mb-5 ms-1">
                                                <input class="form-check-input me-3" name="checkbox_como_llego" type="checkbox" value="0" id="checkbox_option_1" />
                                                <label class="form-check-label" for="checkbox_option_1">
                                                    <div class="fw-semibold text-gray-800">Web</div>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-custom form-check-solid mb-5 ms-1">
                                                <input class="form-check-input me-3" name="checkbox_como_llego" type="checkbox" value="1" id="checkbox_option_2" />
                                                <label class="form-check-label" for="checkbox_option_2">
                                                    <div class="fw-semibold text-gray-800">TV</div>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-custom form-check-solid mb-5 ms-1">
                                                <input class="form-check-input me-3" name="checkbox_como_llego" type="checkbox" value="2" id="checkbox_option_3" />
                                                <label class="form-check-label" for="checkbox_option_3">
                                                    <div class="fw-semibold text-gray-800">Redes sociales</div>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-custom form-check-solid mb-5 ms-1">
                                                <input class="form-check-input me-3" name="checkbox_como_llego" type="checkbox" value="3" id="checkbox_option_4" />
                                                <label class="form-check-label" for="checkbox_option_4">
                                                    <div class="fw-semibold text-gray-800">Amigo</div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Campos de datos-->
                                </div>

								<!--begin::Actions-->
								<div class="d-flex flex-stack pt-15 justify-content-center">
                                    <button id="votacion_submit" type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="submit">
                                        <span class="indicator-label">Realizar votación
                                            <span class="svg-icon svg-icon-3 ms-2 me-0">
                                                <i class="bi bi-save"></i>
                                            </span>
                                        </span>
                                        <span class="indicator-progress">Procesando...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
								</div>
								<!--end::Actions-->
							</form>
							<!--end::Form-->
						</div>
					</div>
					<!--begin::Footer-->
                    <div id="kt_app_footer" class="app-footer">
                        <div class="app-container  container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3 ">
                            <div class="text-gray-900 order-2 order-md-1">
                                © <script>document.write(new Date().getFullYear())</script>, Derechos de Autor de <a class="text-primary ps-2" href="https://www.miwebcrea.cl" target="_blank"> MiWebCrea</a> <a href="https://www.miwebcrea.cl" target="_blank" rel="external nofollow"> <img src="./recursos/web/media/logos/web-2.png" class="logo-web" onmouseover="this.src='./recursos/web/media/logos/web-1.png'" onmouseout="this.src='./recursos/web/media/logos/web-2.png'"></a>
                            </div>
                            <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
                                <li class="menu-item">
                                    <a href="./index" class="menu-link px-2">Inicio</a>
                                </li>
                                <li class="menu-item">
                                    <a href="./tablas" class="menu-link px-2">Tablas</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end::Footer-->
				</div>
				<!--end::Grid Central-->
			</div>
			<!--end::Votacion-->
		</div>
        <div class="engage-toolbar d-flex position-fixed px-5 fw-bolder zindex-2 top-50 end-0 transform-90 mt-20 gap-2">
			<button id="kt_help_toggle" class="engage-help-toggle btn btn-flex h-35px bg-body btn-color-gray-700 btn-active-color-gray-900 shadow-sm px-5 rounded-top-0" title="" data-bs-toggle="tooltip" data-bs-placement="left" data-bs-dismiss="click" data-bs-trigger="hover" data-bs-original-title="Para ver los enlaces y documentos relacionados a este proyecto. Pincha aquí para más info.">Info y más</button>
		</div>
        <div id="kt_help" class="bg-body drawer drawer-end" data-kt-drawer="true" data-kt-drawer-name="help" data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'350px', 'md': '525px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_help_toggle" data-kt-drawer-close="#kt_help_close" style="width: 525px !important;">
			<div class="card shadow-none rounded-0 w-100">
				<div class="card-header" id="kt_help_header">
					<h5 class="card-title fw-bold text-gray-600">Enlaces y más</h5>
					<div class="card-toolbar">
						<button type="button" class="btn btn-sm btn-icon explore-btn-dismiss me-n5" id="kt_help_close">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
								</svg>
							</span>
						</button>
					</div>
				</div>
				<div class="card-body" id="kt_help_body">
					<div id="kt_help_scroll" class="hover-scroll-overlay-y" data-kt-scroll="true" data-kt-scroll-height="auto" data-kt-scroll-wrappers="#kt_help_body" data-kt-scroll-dependencies="#kt_help_header" data-kt-scroll-offset="5px" style="height: 311px;">

						<div class="d-flex align-items-center mb-7">
							<div class="d-flex flex-center w-50px h-50px w-lg-75px h-lg-75px flex-shrink-0 rounded bg-lighten">
                                <a href="https://www.desis.cl/v3/20230220dev.pdf" class="text-primary" target="_blank" rel="noopener noreferrer">
                                    <span class="svg-icon svg-icon-primary svg-icon-2x svg-icon-lg-3x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-filetype-pdf" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M14 4.5V14a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V4.5h-2A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v9H2V2a2 2 0 0 1 2-2h5.5zM1.6 11.85H0v3.999h.791v-1.342h.803c.287 0 .531-.057.732-.173.203-.117.358-.275.463-.474a1.42 1.42 0 0 0 .161-.677c0-.25-.053-.476-.158-.677a1.176 1.176 0 0 0-.46-.477c-.2-.12-.443-.179-.732-.179Zm.545 1.333a.795.795 0 0 1-.085.38.574.574 0 0 1-.238.241.794.794 0 0 1-.375.082H.788V12.48h.66c.218 0 .389.06.512.181.123.122.185.296.185.522Zm1.217-1.333v3.999h1.46c.401 0 .734-.08.998-.237a1.45 1.45 0 0 0 .595-.689c.13-.3.196-.662.196-1.084 0-.42-.065-.778-.196-1.075a1.426 1.426 0 0 0-.589-.68c-.264-.156-.599-.234-1.005-.234H3.362Zm.791.645h.563c.248 0 .45.05.609.152a.89.89 0 0 1 .354.454c.079.201.118.452.118.753a2.3 2.3 0 0 1-.068.592 1.14 1.14 0 0 1-.196.422.8.8 0 0 1-.334.252 1.298 1.298 0 0 1-.483.082h-.563v-2.707Zm3.743 1.763v1.591h-.79V11.85h2.548v.653H7.896v1.117h1.606v.638H7.896Z"/>
                                        </svg>
                                    </span>
                                </a>
							</div>

							<div class="d-flex flex-stack flex-grow-1 ms-4 ms-lg-6">
								<div class="d-flex flex-column me-2 me-lg-5">
									<a href="javascript:void(0)" class="text-dark text-hover-primary fw-bolder fs-6 fs-lg-4 mb-1"></a>
									<div class="text-muted fw-bold fs-7 fs-lg-6">PDF del enunciado de la prueba de diagnóstico presentado por Desis.</div>
								</div>
							</div>
						</div>

                        <div class="d-flex align-items-center mb-7">
							<div class="d-flex flex-center w-50px h-50px w-lg-75px h-lg-75px flex-shrink-0 rounded bg-lighten">
                                <a href="https://drive.google.com/file/d/1kNBXkKfF5SyCmAduWND7FC_KcOYChsZL/view?usp=sharing" class="text-primary" target="_blank" rel="noopener noreferrer">
                                    <span class="svg-icon svg-icon-primary svg-icon-2x svg-icon-lg-3x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-diagram-2-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6 3.5A1.5 1.5 0 0 1 7.5 2h1A1.5 1.5 0 0 1 10 3.5v1A1.5 1.5 0 0 1 8.5 6v1H11a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0v-1A.5.5 0 0 1 5 7h2.5V6A1.5 1.5 0 0 1 6 4.5zm-3 8A1.5 1.5 0 0 1 4.5 10h1A1.5 1.5 0 0 1 7 11.5v1A1.5 1.5 0 0 1 5.5 14h-1A1.5 1.5 0 0 1 3 12.5zm6 0a1.5 1.5 0 0 1 1.5-1.5h1a1.5 1.5 0 0 1 1.5 1.5v1a1.5 1.5 0 0 1-1.5 1.5h-1A1.5 1.5 0 0 1 9 12.5z"/>
                                        </svg>
                                    </span>
                                </a>
							</div>

							<div class="d-flex flex-stack flex-grow-1 ms-4 ms-lg-6">
								<div class="d-flex flex-column me-2 me-lg-5">
									<a href="javascript:void(0)" class="text-dark text-hover-primary fw-bolder fs-6 fs-lg-4 mb-1"></a>
									<div class="text-muted fw-bold fs-7 fs-lg-6">Diagrama de la base de datos, en este caso, fue elaborada con SleekDB, base de datos NoSQL implementada utilizando PHP puro, la cual almacena los datos en archivos planos de tipo JSON, muy similar a MongoDB.</div>
								</div>
							</div>
						</div>

                        <div class="d-flex align-items-center mb-7">
							<div class="d-flex flex-center w-50px h-50px w-lg-75px h-lg-75px flex-shrink-0 rounded bg-lighten">
                                <a href="https://gitlab.com/miwebcrea/sistema-votacion" class="text-primary" target="_blank" rel="noopener noreferrer">
                                    <span class="svg-icon svg-icon-primary svg-icon-2x svg-icon-lg-3x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gitlab" viewBox="0 0 16 16">
                                            <path d="m15.734 6.1-.022-.058L13.534.358a.568.568 0 0 0-.563-.356.583.583 0 0 0-.328.122.582.582 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.673.673 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.046 4.046 0 0 0 1.34-4.668Z"/>
                                        </svg>
                                    </span>
                                </a>
							</div>

							<div class="d-flex flex-stack flex-grow-1 ms-4 ms-lg-6">
								<div class="d-flex flex-column me-2 me-lg-5">
									<a href="javascript:void(0)" class="text-dark text-hover-primary fw-bolder fs-6 fs-lg-4 mb-1"></a>
									<div class="text-muted fw-bold fs-7 fs-lg-6">Directorio de GitLab con todos los archivos del sistema de votación.</div>
								</div>
							</div>
						</div>

                        <div class="d-flex align-items-center mb-7">
							<div class="d-flex flex-center w-50px h-50px w-lg-75px h-lg-75px flex-shrink-0 rounded bg-lighten">
                                <a href="https://gitlab.com/miwebcrea/sistema-votacion/-/blob/main/README.md?ref_type=heads" class="text-primary" target="_blank" rel="noopener noreferrer">
                                    <span class="svg-icon svg-icon-primary svg-icon-2x svg-icon-lg-3x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-filetype-md" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M14 4.5V14a2 2 0 0 1-2 2H9v-1h3a1 1 0 0 0 1-1V4.5h-2A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v9H2V2a2 2 0 0 1 2-2h5.5zM.706 13.189v2.66H0V11.85h.806l1.14 2.596h.026l1.14-2.596h.8v3.999h-.716v-2.66h-.038l-.946 2.159h-.516l-.952-2.16H.706Zm3.919 2.66V11.85h1.459c.406 0 .741.078 1.005.234.263.157.46.383.589.68.13.297.196.655.196 1.075 0 .422-.066.784-.196 1.084-.131.301-.33.53-.595.689-.264.158-.597.237-1 .237H4.626Zm1.353-3.354h-.562v2.707h.562c.186 0 .347-.028.484-.082a.8.8 0 0 0 .334-.252 1.14 1.14 0 0 0 .196-.422c.045-.168.067-.365.067-.592a2.1 2.1 0 0 0-.117-.753.89.89 0 0 0-.354-.454c-.159-.102-.362-.152-.61-.152Z"/>
                                        </svg>
                                    </span>
                                </a>
							</div>

							<div class="d-flex flex-stack flex-grow-1 ms-4 ms-lg-6">
								<div class="d-flex flex-column me-2 me-lg-5">
									<a href="javascript:void(0)" class="text-dark text-hover-primary fw-bolder fs-6 fs-lg-4 mb-1"></a>
									<div class="text-muted fw-bold fs-7 fs-lg-6">Enlace al LEEME.md que contiene las instrucciones para visualizar el sistema de votación.</div>
								</div>
							</div>
						</div>
					</div>
					<!--end::Content-->
				</div>
				<!--end::Body-->
			</div>
			<!--end::Card-->
		</div>
		<!--end::Principal-->
		<script>var hostUrl = "recursos/web/";</script>
		<!--begin::Global Javascript usado en todas las paginas-->
		<script src="recursos/web/plugins/global/plugins.bundle.js"></script>
		<script src="recursos/web/js/scripts.bundle.js"></script>
        <script src="recursos/web/js/widgets.js"></script>
		<!--end::Global Javascript usado en todas las paginas-->
		<!--begin::Javascript Personalizado usado en esta pagina-->
		<script src="recursos/web/js/custom/pages/validation/votacion-form.js"></script>
		<script src="recursos/web/js/custom/pages/votacion.js"></script>
		<!--end::Javascript Personalizado usado en esta pagina-->
	</body>
	<!--end::Body-->
</html>