<?php
class Paises_model
{
    public function __construct()
    {
        // include "../Controlador/Parametros_globales.php";
        include_once "../recursos/SleekBD/Store.php";
        $this->dirBD = '../BD';
    }

    /**
     * Comprime en un arreglo el contenido de Secciones
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   valor
     * @return  encryption
    */
    public function getPaisesAll()
    {
        $seccionesBD = new \SleekDB\Store('tblpaises', $this->dirBD);
        $userQueryBuilder = $seccionesBD->createQueryBuilder();
        $paisesAll = $userQueryBuilder
            ->getQuery()
            ->fetch();
        return $paisesAll;
    }
}
?>