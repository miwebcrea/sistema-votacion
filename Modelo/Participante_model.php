<?php
class Participante_model
{
    public function __construct()
    {
        include_once "../recursos/SleekBD/Store.php";
        $this->dirBD = '../BD';
    }

    /**
     * Obtenemos todos los participantes
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @return  array
    */
    public function getParticipantesTodos()
    {
        $arrayParticipante = new \SleekDB\Store('tblparticipantes', $this->dirBD);
        $userQueryBuilder = $arrayParticipante->createQueryBuilder();
        $arrayParticipante = $userQueryBuilder
            ->orderBy(["_id" => "desc"])
            ->getQuery()
            ->fetch();
        return $arrayParticipante;
    }

    /**
     * Obtenemos el participante mediante el correo
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   correo
     * @return  arrayParticipante
    */
    public function getUsuarioPorCorreo($correo)
    {
        $arrayParticipante = new \SleekDB\Store('tblparticipantes', $this->dirBD);
        $userQueryBuilder = $arrayParticipante->createQueryBuilder();
        $arrayParticipante = $userQueryBuilder
            ->where( ["correo_electronico", "=", $correo] )
            ->getQuery()
            ->fetch();
        return $arrayParticipante;
    }

    /**
     * Obtenemos el participante mediante el rut
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   rut
     * @return  arrayParticipante
    */
    public function getParticipantePorRut($rut)
    {
        $clientesBD = new \SleekDB\Store('tblparticipantes', $this->dirBD);
        $userQueryBuilder = $clientesBD->createQueryBuilder();
        $arrayParticipante = $userQueryBuilder
            ->where( ["rut", "=", $rut] )
            ->getQuery()
            ->fetch();
        return $arrayParticipante;
    }

    /**
     * Creamos un nuevo registro de participante para una votacion
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   array
     * @return  array
    */
    public function insertParticipante($array)
    {
        $arrayParticipante = new \SleekDB\Store('tblparticipantes', $this->dirBD);
        $resultado = $arrayParticipante->insert($array);

        return $resultado;
    }
}
?>