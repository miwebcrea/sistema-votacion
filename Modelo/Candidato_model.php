<?php
class Candidato_model
{
    public function __construct()
    {
        include_once "../recursos/SleekBD/Store.php";
        $this->dirBD = '../BD';
    }

    /**
     * Obtenemos todos los candidatos
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   valor
     * @return  encryption
    */
    public function getCandidatosTodos()
    {
        $seccionesBD = new \SleekDB\Store('tblcandidatos', $this->dirBD);
        $userQueryBuilder = $seccionesBD->createQueryBuilder();
        $candidatosAll = $userQueryBuilder
            ->getQuery()
            ->fetch();
        return $candidatosAll;
    }
}
?>