<?php
class Votacion_model
{
    public function __construct()
    {
        include_once "../recursos/SleekBD/Store.php";
        $this->dirBD = '../BD';
    }

    /**
     * Obtenemos todos las votaciones
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @return  array
    */
    public function getVotacionesTodas()
    {
        $arrayVotacion = new \SleekDB\Store('tblvotacion', $this->dirBD);
        $userQueryBuilder = $arrayVotacion->createQueryBuilder();
        $arrayVotacion = $userQueryBuilder
            ->orderBy(["_id" => "desc"])
            ->getQuery()
            ->fetch();
        return $arrayVotacion;
    }

    /**
     * Creamos un nuevo registro de votacion
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   array
     * @return  array
    */
    public function insertVotacion($array)
    {
        $arrayVotacion = new \SleekDB\Store('tblvotacion', $this->dirBD);
        $resultado = $arrayVotacion->insert($array);

        return $resultado;
    }
}
?>