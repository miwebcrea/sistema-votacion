<?php
class Ubicacion_model
{
    public function __construct()
    {
        include_once "../recursos/SleekBD/Store.php";
        $this->dirBD = '../BD';
    }

    /**
     * Devuelve la comuna por el Id
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   id
     * @return  array
    */
    public function getComunaPorIDReg($id)
    {
        $comunasBD = new \SleekDB\Store('tblcomunas', $this->dirBD);
        $comuna = $comunasBD->findById($id);

        return $comuna;
    }

    /**
     * Obtenemos todos los datos de la tabla
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   valor
     * @return  encryption
    */
    public function getRegionesTodas()
    {
        $regionesBD = new \SleekDB\Store('tblregiones', $this->dirBD);
        $userQueryBuilder = $regionesBD->createQueryBuilder();
        $regionesAll = $userQueryBuilder
            ->orderBy(["_id" => "asc"])
            ->getQuery()
            ->fetch();
        return $regionesAll;
    }

    /**
     * Obtenemos todos los datos de la tabla
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   valor
     * @return  encryption
    */
    public function getComunasTodas()
    {
        $regionesBD = new \SleekDB\Store('tblcomunas', $this->dirBD);
        $userQueryBuilder = $regionesBD->createQueryBuilder();
        $regionesAll = $userQueryBuilder
            ->orderBy(["_id" => "asc"])
            ->getQuery()
            ->fetch();
        return $regionesAll;
    }

    /**
     * Obtenemos la region por el cod_region
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   cod_region
     * @return  array
    */
    public function getRegionPorCod($cod_region)
    {
        $regionesBD = new \SleekDB\Store('tblregiones', $this->dirBD);
        $userQueryBuilder = $regionesBD->createQueryBuilder();
        $region = $userQueryBuilder
            ->where( ["cod_region", "=", $cod_region] )
            ->getQuery()
            ->fetch();
        return $region;
    }

    /**
     * Obtenemos la comuna por el id de la region
     *
     * @author  Michael Quiroz
     * @since   Version 1.0.0
     * @version 1.0
     * @param   id_region
     * @return  array
    */
    public function getComunaPorRegion($id_region)
    {
        $comunasBD = new \SleekDB\Store('tblcomunas', $this->dirBD);
        $userQueryBuilder = $comunasBD->createQueryBuilder();
        $comunas = $userQueryBuilder
            ->where( ["fk_region", "=", $id_region] )
            ->orderBy(["nombre" => "asc"])
            ->getQuery()
            ->fetch();
        return $comunas;
    }
}
?>