## Tecnologías

- HTML5
- CSS
- Materialize
- Bootstrap 5
- Bootstrap Drawer
- Bootstrap Spinners
- Sass
- JQuery
- Smooth Scroll
- Select2
- Inputmask
- FormValidation
- SweetAlert
- DataTable

## Base de datos

En este caso se utilizó SleekDB, la cual es una base de datos no relacional, elaborada con PHP puro. Su sistema se define como archivos planos de tipo JSON, muy similar a MongoDB.

## Diagrama de base de datos

![alt text](https://lh3.googleusercontent.com/drive-viewer/AEYmBYTbobb3RBLoU1rWsE4CsddxIUr5eT_qbG5nH8k0dYLOQKSxiYhTOS_JU_JJJ9cUovtEpXQKjOJwRwmbbn4fBC4XnJxp9w=w2560-h1307)
https://drive.google.com/file/d/1kNBXkKfF5SyCmAduWND7FC_KcOYChsZL/view?usp=sharing

## Instalación

*Para este proyecto se optó por un desarrollo que no necesita una instalación avanzada, solo basta con integrar el repositorio en un servidor local o web.*

### Prerrequisitos

El único requisito para el correcto funcionamiento es tener la versión 7.4 de PHP.

### Paso a paso

1. Clona este repositorio usando git clone `https://gitlab.com/miwebcrea/sistema-votacion.git`
1. Accede al sistema a travez de su URL, si esta en un servidor local será `https://localhost/sistema-votacion/votacion`

## Anotaciones

Este proyecto ya está operativo en un servidor web de mi propiedad; para acceder a él debes dirigirte al siguiente enlace https://miwebcrea.cl/integraciones/sistema-votacion/votacion
